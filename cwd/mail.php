<?php
	include("php-mailjet-v3-simple.class.php");
	
	function sendEmail(){
    	$to = 'info@limbo.lu';
    	$from = "info@limbo.lu";
	    $headers = "Content-type: text/plain; charset=utf-8\r\nFrom: info@limbo.lu";
		$subject = 'Limbo Restoclub';
		$subject2 = 'Thank you for considering Limbo Restoclub for your next amazing event.';
	    $mj = new Mailjet( '843c3d5e151a34b7b8554ce1b4bfe294', '6bfff02c1c52611708bbce21eabdf31a' );
	    
    	$message = "Name: ".$_POST['name'];
    	if($_POST['company'])
    	{
    		$message .= "\r\nCompany: ".$_POST['company'];
    	}
    	if($_POST['email'])
    	{
    		$message .= "\r\nEmail: ".$_POST['email'];
    	}
    	if($_POST['tel'])
    	{
    		$message .= "\r\nPhone: ".$_POST['tel'];
    	}
    	if($_POST['select-currancy'])
    	{
    		$message .= "\r\nParty Type: ".$_POST['select-currancy'];
    	}
    	if($_POST['select-currancy2'])
    	{
    		$message .= "\r\nParty Size: ".$_POST['select-currancy2'];
    	}
    	if($_POST['date'])
    	{
    		$message .= "\r\nDate: ".$_POST['date'];
    	}
    	if($_POST['message'])
    	{
    		$message .= "\r\nMessage: ".$_POST['message'];
    	}
    	if($_POST['email'])
    	{
    		$to = $_POST['email'];
    	}
    	
        $params = array(
            "method" => "POST",
            "from" => $from,
            "to" => "e.alanev@yandex.ru",
            "subject" => $subject,
            "text" => $message
        );
    
        $result = $mj->sendEmail($params);
        
        if ($mj->_response_code == 200)
           echo "success - email sent";
        else
           echo "error - ".$mj->_response_code;
    
        return $result;
    }
    
	function sendEmailWithAttachments(){
    	$to = 'e.alanev@yandex.ru';
    	$from = "info@limbo.lu";
	    $headers = "Content-type: text/plain; charset=utf-8\r\nFrom: info@limbo.lu";
		$subject = 'Limbo Restoclub';
		$subject2 = 'Thank you for considering Limbo Restoclub for your next amazing event.';
	    $mj = new Mailjet( '843c3d5e151a34b7b8554ce1b4bfe294', '6bfff02c1c52611708bbce21eabdf31a' );
	    
    	$message = "Name: ".$_POST['name'];
    	if($_POST['company'])
    	{
    		$message .= "\r\nCompany: ".$_POST['company'];
    	}
    	if($_POST['email'])
    	{
    		$message .= "\r\nEmail: ".$_POST['email'];
    	}
    	if($_POST['tel'])
    	{
    		$message .= "\r\nPhone: ".$_POST['tel'];
    	}
    	if($_POST['select-currancy'])
    	{
    		$message .= "\r\nParty Type: ".$_POST['select-currancy'];
    	}
    	if($_POST['select-currancy2'])
    	{
    		$message .= "\r\nParty Size: ".$_POST['select-currancy2'];
    	}
    	if($_POST['date'])
    	{
    		$message .= "\r\nDate: ".$_POST['date'];
    	}
    	if($_POST['message'])
    	{
    		$message .= "\r\nMessage: ".$_POST['message'];
    	}
    	if($_POST['email'])
    	{
    		$to = $_POST['email'];
    	}
    	
        $params = array(
            "method" => "POST",
            "from" => $from,
            "to" => $to,
            "subject" => $subject,
            "text" => $message,
            "attachment" => array(
                "@file.pdf"
                )
        );
    
        $result = $mj->sendEmail($params);
        
        if ($mj->_response_code == 200)
           echo "success - email sent";
        else
           echo "error - ".$mj->_response_code;
    
        return $result;
    }
    sendEmail();
    sendEmailWithAttachments();
?>