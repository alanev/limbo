var page = $('.g-page'),
	pageWidth = page.width(),
	popup = {
		activeClass: 'open',
		open: function (popupName) {
			page.addClass('lock').css({ 'margin-right': page.width() - pageWidth });
			document.querySelector('[data-popup="' + popupName + '"]').classList.add(this.activeClass);
		},
		close: function (popup) {
			page.removeClass('lock').css({ 'margin-right': '' });
			popup.removeClass(this.activeClass);
			history.replaceState('', '', location.pathname);
		}
	};

$('.popup__close, .popup__button,.popup__overlay').click(function () {
	popup.close($(this).parents('.popup'));
});