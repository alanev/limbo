var activePageClass = 'page_active',
	pageToggle = function(){
		var pageName = location.hash.replace('#', '');
		if(pageName === ''){
			$('.page_main').addClass(activePageClass);
			$('.page_inner').removeClass(activePageClass);
		}else{
			$('.page_main').removeClass(activePageClass);
			$('.page_' + pageName ).addClass(activePageClass).siblings().removeClass(activePageClass);
			$('.page_' + pageName + ' .slider').owlCarousel(owlOptions)
		}
	};
window.addEventListener('hashchange', pageToggle);
window.addEventListener('load', pageToggle);