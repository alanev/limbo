$('form').submit(function () {
	var form = $(this);
	$.ajax({
		url: form.attr('action'),
		method: form.attr('method'),
		dataType: 'html',
		data: form.serialize(),
		success: function (response) {
			$('.popup').removeClass('open');
			popup.open('thanks');
			setTimeout(function() { $('.popup').removeClass('open'); }, 15000);
		},
		error: function (response) {
			alert('Ошибка при отправке формы');
		}
	});
	return false;
});
$(document).ready(function () {
	$('.form__input[type=tel]').mask('##########');
});

$('.form__input_date').pickmeup({
	position		: 'top',
	hide_on_select	: true
});